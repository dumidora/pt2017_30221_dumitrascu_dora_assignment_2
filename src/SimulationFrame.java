import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

public class SimulationFrame extends JFrame{
	private JPanel panel; //main frame will be split in 2 sections
	private JPanel upperPanel;
	private JPanel timePanel;
	
	private JTextField minArrTime = new JTextField();
	private JTextField maxArrTime = new JTextField();
	private JTextField minProcTime = new JTextField();
	private JTextField maxProcTime = new JTextField();
	private JTextField noOfServers = new JTextField();
	private JTextField noOfTasks = new JTextField();
	private JTextField simTime = new JTextField();
	private JButton startButton = new JButton("Start");
	private boolean clickStatus = false;
	private static String[] options = {"SHORTEST_TIME","SHORTEST_QUEUE"};
	private static JComboBox cbox = new JComboBox (options);
	private static boolean validate = false;
	static String minAT=null;
	static String maxAT=null;
	static String minPT=null;
	static String maxPT=null;
	static String noOfS=null;
	static String noOfT=null;
	static String sim = null;
	
	
	private int WIDTH = 1200, HEIGHT = 600;
	private DefaultListModel<Integer> dataModel = new DefaultListModel<>();
	
	public SimulationFrame(){
		startButton.addMouseListener(new StartButtonListener());
		this.setLayout(new BorderLayout());
		panel = new JPanel();
		upperPanel = new JPanel();
		timePanel = new JPanel();
		JPanel p1 = new JPanel();
		p1.setLayout(new BoxLayout(p1,BoxLayout.PAGE_AXIS));
		p1.add(new JLabel("Minimum arrival time:"));
		p1.add(minArrTime);
		upperPanel.add(p1);
		
		JPanel p2 = new JPanel();
		p2.setLayout(new BoxLayout(p2,BoxLayout.PAGE_AXIS));
		p2.add(new JLabel("Maximum arrival time:"));
		p2.add(maxArrTime);
		upperPanel.add(p2);
		
		JPanel p3 = new JPanel();
		p3.setLayout(new BoxLayout(p3,BoxLayout.PAGE_AXIS));
		p3.add(new JLabel("Minimum processing time:"));
		p3.add(minProcTime);
		upperPanel.add(p3);
		
		JPanel p4 = new JPanel();
		p4.setLayout(new BoxLayout(p4,BoxLayout.PAGE_AXIS));
		p4.add(new JLabel("Maximum processing time:"));
		p4.add(maxProcTime);
		upperPanel.add(p4);
		
		JPanel p5 = new JPanel();
		p5.setLayout(new BoxLayout(p5,BoxLayout.PAGE_AXIS));
		p5.add(new JLabel("Number of Servers:"));
		p5.add(noOfServers);
		upperPanel.add(p5);
		
		JPanel p6 = new JPanel();
		p6.setLayout(new BoxLayout(p6,BoxLayout.PAGE_AXIS));
		p6.add(new JLabel("Number of Tasks:"));
		p6.add(noOfTasks);
		upperPanel.add(p6);
		
		JPanel p7 = new JPanel();
		p7.setLayout(new BoxLayout(p7,BoxLayout.PAGE_AXIS));
		p7.add(new JLabel("Simulation time:"));
		p7.add(simTime);
		upperPanel.add(p7);
		
		
		JPanel p8 = new JPanel();
		p8.setLayout(new BoxLayout(p8,BoxLayout.PAGE_AXIS));
		p8.add(new JLabel("Simulation strategy:"));
		p8.add(cbox);
		upperPanel.add(p8);
		
		upperPanel.add(startButton);
		
		this.add(upperPanel, BorderLayout.NORTH);
		this.add(panel,BorderLayout.CENTER);
		this.add(timePanel,BorderLayout.SOUTH);
		
		
		this.setSize(WIDTH,HEIGHT);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		this.setVisible(true);
	}
	
	public void DisplaySimulationData(){
		this.setSize(300,300);
		panel.removeAll();
		upperPanel.removeAll();
		upperPanel.revalidate();
		panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
		
		panel.add(new JLabel("Maximum waiting time was: "+Logger.maxWT));
		panel.add(new JLabel("Average waiting time was: "+Logger.avgWT));
		panel.add(new JLabel("Starting moment of peak hour: "+Logger.peakHourStart));
		panel.add(new JLabel("End of peak hour: "+Logger.peakHourFinish));
		panel.add(new JLabel("Time after all tasks have been processed: "+Logger.simulationFinish));
		panel.revalidate();
		
	}
	
	/*
	 * Display ServerNumber JLabels and JScrollPanes with JLists
	 */
	
	public void DisplayServers(int serverNumber, Task[][] taskArr, int currentTime){
		panel.setLayout(new GridLayout(2,serverNumber));
		panel.removeAll();
		timePanel.removeAll();
		timePanel.add(new JLabel("Time Passed: "+String.valueOf(currentTime)));
		timePanel.revalidate();
		panel.revalidate();
		
		JPanel[][] panelHolder = new JPanel[2][serverNumber];
		//we will have two rows, one for labels and one for scrollpanes
		
		
		for(int i=0;i<2;i++){//i = number of rows, serverNumber = number of cols
			
			
			for(int j=0;j<serverNumber;j++){
				JLabel label = new JLabel("Server number "+(j+1));
				
				panelHolder[i][j]=new JPanel();
				if(i==0){
					panelHolder[i][j].add(label);
				}//upper half will be the server names
				if(i==1){
					DefaultListModel listModel = new DefaultListModel();
					JList taskList = new JList(listModel);
					JScrollPane taskPane = new JScrollPane(taskList);
					
					for(int k=0;k<taskArr[j].length;k++){ //iterate through server tasks
						listModel.addElement(taskArr[j][k]);
					}
					panelHolder[i][j].add(taskPane);
				}
				
				panel.add(panelHolder[i][j]);
				panelHolder[i][j].setVisible(true);
			}
			
		}
		
		revalidate();
		
	}
	/*
	 * Set values in SimulationManager
	 */
	public static int getMinAT(){return Integer.parseInt(minAT);}
	public static int getMaxAT(){return Integer.parseInt(maxAT);}
	public static int getMinPT(){return Integer.parseInt(minPT);}
	public static int getMaxPT(){return Integer.parseInt(maxPT);}
	public static int getNoOfS(){return Integer.parseInt(noOfS);}
	public static int getNoOfT(){return Integer.parseInt(noOfT);}
	public static int getSim(){return Integer.parseInt(sim);}
	
	
	public static boolean returnClickStatus(){
		return validate;
	}
	
	public static String getOption(){
		return (String)cbox.getSelectedItem();
	}
	
	private class StartButtonListener implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			if(!validate){
				/*
				 * Check that we have input for each value!
				 */
				validate = true;
				if(minArrTime.getText().trim().isEmpty() ||
				   maxArrTime.getText().trim().isEmpty() ||
				   minProcTime.getText().trim().isEmpty() ||
				   maxProcTime.getText().trim().isEmpty()||
				   noOfServers.getText().trim().isEmpty()||
				   noOfTasks.getText().trim().isEmpty()||
				   simTime.getText().trim().isEmpty()){
					validate=false;
				}
					
				if(!validate)
					JOptionPane.showMessageDialog(new JFrame(), "Empty field!");
				else{
				minAT = minArrTime.getText();
				maxPT = maxProcTime.getText();
				noOfT = noOfTasks.getText();
				sim = simTime.getText();
				maxAT = maxArrTime.getText();
				minPT = minProcTime.getText();
				noOfS = noOfServers.getText();
				System.out.println(minAT+" "+maxAT+" "+minPT+" "+maxPT+" "+noOfS+" "+noOfT+" "+sim);
				System.out.println("CLICKED");
				validate=true;
			}
		}
				
	}

		@Override
		public void mouseEntered(MouseEvent arg0) {}
		@Override
		public void mouseExited(MouseEvent arg0) {}
		@Override
		public void mousePressed(MouseEvent arg0) {}
		@Override
		public void mouseReleased(MouseEvent arg0) {	}
		
	}
}
