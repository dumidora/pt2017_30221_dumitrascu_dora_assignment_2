import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class SimulationManager implements Runnable{
	public static int timeLimit=10;
	public static int minArrTime=0;
	public static int maxArrTime=10;
	public static int maxProcTime=10;
	public static int minProcTime=1;
	public static int numberOfServers=4;
	public static int numberOfClients=15;
	public SelectionPolicy selectionPolicy;
	private Scheduler scheduler;
	private SimulationFrame frame;
	private List<Task> generatedTasks;
	private Task[][] taskArray; 
	/*
	 * taskArray will be the parameter in the Display method from the SimulationFrame
	 */
	
	public SimulationManager(){
		
		frame = new SimulationFrame();
		int choice=1;
		/*
		 * choice is the Strategy we choose 
		 * 1 = Shortest Time
		 * 2 = Shortest Queue
		 */
		
		/*Set the strategy
		 *Here for test, it is 1 by default
		 */
		/*if(choice==1)
			selectionPolicy =  SelectionPolicy.SHORTEST_TIME;
		else
			selectionPolicy = SelectionPolicy.SHORTEST_QUEUE;*/
	}
	
	private void showTasks(){
		int i=0;
		for(Task t : generatedTasks){
			System.out.println("Task number "+i+" aTime="+ t.getArrTime()+" ; pTime="+t.getProcTime());
			i++;
		}

	}
	
	private void generateNRandomTasks(){ //O(n)
		Random rand = new Random();
		generatedTasks = new ArrayList<Task>();
		for(int i=0;i<numberOfClients;i++){
			int randProcTime = rand.nextInt((maxProcTime-minProcTime)+1)+minProcTime;
			int randArrTime = rand.nextInt((maxArrTime-minArrTime)+1)+minProcTime;
			generatedTasks.add(new Task(randArrTime,randProcTime)); 
		}
		

		
	}
	
	@Override
	public void run() {
		int currentTime = 0;
		while(currentTime<timeLimit){  //O(timeLimit * generatedTasks.size())
			System.out.println("--CURRENT TIME-- "+currentTime);
			for(Iterator<Task> it = generatedTasks.iterator();it.hasNext();){ 
				Task t = it.next();
				if(t.getArrTime()==currentTime){
					System.out.println("I am inserting a task!"+t.toString());
					scheduler.dispatchTask(t);
					it.remove(); //concurrentModificationExepction ->avoid by using remove() from Iterator, not ArrayList!
				}
			}
			
			System.out.println("Servers after this second:");
			int i=0;
			//we will check every second for a minimum waiting time, at end of simulation we will have a variable that tells us
			Logger.maxWaitingTime(scheduler.getServers());
			Logger.avgWaitingTime(scheduler.getServers());
			Logger.peakHour(scheduler.getServers(),currentTime);
			
			
			for(Server m : scheduler.getServers()){
				taskArray[i]=m.getTasks();
				System.out.println(m.toString());
				i++;
			}
			
			//here try to show it
			frame.DisplayServers(numberOfServers, taskArray,currentTime);
			currentTime++;
			try {
				Thread.sleep(1000); //sleep 1 second
				System.out.println("I slept 1 sec!");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		while(!checkAllServers()){
			System.out.println("Servers after this second:");		
			/*
			 * Increment currentTime here, so we can see for
			 * how long the simulation continues if there are any more tasks left
			 * after the end of the simulation Time
			 */
			
			System.out.println("not done");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			int j=0;
			for(Server m : scheduler.getServers()){
				
				
				
				for(int i=0;i<m.getTasks().length;i++){
					if(m.getTasks()[i].getArrTime()==currentTime){
						m.removeTask(m.getTasks()[i]);
					}
					
				}
				taskArray[j]=m.getTasks();
				j++;
				
				
				if(m.getWaitingPeriod().get()==0){
					m.shutDown();
				}
			}
			frame.DisplayServers(numberOfServers, taskArray,++currentTime);
		}
		Logger.setFinishTime(currentTime);
		
		frame.DisplaySimulationData();
	}
	
	
	public boolean checkAllServers(){
		
		for(Server m : scheduler.getServers()){
				if(m.getWaitingPeriod().get()==0){ 
					
					System.out.println("SHUTTING DOWN SERVER");
					m.shutDown();		
				}
				else
					return false;
						//if we find a server that is not done, we return false, that means simulation still going
		}
		return true;
	}

		
	public static void main(String[] args) throws InterruptedException{
		SimulationManager gen = new SimulationManager();
		/*We wait until we press the Start button
		 */
		boolean status=false;
		while((status=SimulationFrame.returnClickStatus())==false){
			System.out.println("not");
			Thread.sleep(1000);
		}
		if(status){
			System.out.println("valid");
			minArrTime=SimulationFrame.getMinAT();
			maxArrTime=SimulationFrame.getMaxAT();
			minProcTime=SimulationFrame.getMinPT();
			maxProcTime=SimulationFrame.getMaxPT();
			numberOfServers=SimulationFrame.getNoOfS();
			numberOfClients=SimulationFrame.getNoOfT();
			timeLimit=SimulationFrame.getSim();
			
			if(SimulationFrame.getOption()=="SHORTEST TIME"){
				gen.selectionPolicy=SelectionPolicy.SHORTEST_TIME;
			}
			else
				gen.selectionPolicy=SelectionPolicy.SHORTEST_QUEUE;
		}
		gen.taskArray = new Task[numberOfServers][];
		gen.scheduler=new Scheduler(numberOfServers,numberOfClients);
		gen.scheduler.changeStrategy(gen.selectionPolicy);
		gen.generateNRandomTasks();
		Collections.sort(gen.generatedTasks);
		
		gen.showTasks();
		Thread t = new Thread(gen);
		
		t.start();
		
		
		
	}
}
