import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ConcreteStrategyQueue implements Strategy{

	@Override
	
	public void addTask(List <Server> servers, Task t){
		int minIndex=0;
		int minSize = servers.get(0).getTasks().length;
		for(Server m : servers){
			if(m.getTasks().length<minSize){
				minIndex=servers.indexOf(m);
			}
		}
		servers.get(minIndex).addTask(t);
	}

}
