import java.util.List;

public class Logger {

	public static int maxWT=0;
	public static int avgWT=0;
	public static int simulationFinish;
	public static int peakHourStart=0;
	public static int peakHourTasks =0;
	public static int peakHourFinish =0;
	
	public static void maxWaitingTime(List<Server> servers){
		int WT =0;
		/*We iterate through all the tasks of each server
		 * and maxWT will be the one with the max waiting time
		 */
		for(Server m : servers){
			Task[] t = m.getTasks();
			
			for(int i=0;i<t.length;i++){
				WT=0;
				for(int j=0;j<i;j++){
					WT=WT+t[j].getProcTime();
					
				}
				t[i].setWaitingTime(WT);
				if(WT>maxWT)
					maxWT=WT;
			}
		}
	}
	
	public static void avgWaitingTime(List<Server> servers){
		int serverAvg = 0;
		for(Server m : servers){
			int tempAvg=0;
			Task[] t = m.getTasks();
			for(int i=0;i<t.length;i++){
				tempAvg = tempAvg+t[i].getWaitingTime();
			}
			if(t.length!=0)
				tempAvg= tempAvg/t.length;
			serverAvg=serverAvg+tempAvg;
		}
		serverAvg = serverAvg/servers.size();
		avgWT=serverAvg;
		
	}
	
	
	public static void peakHour(List<Server> servers, int time){
		int taskNo=0;
		for(Server m : servers){
			taskNo=taskNo+m.getTasks().length;
		}
		if(taskNo>peakHourTasks){
			peakHourStart=time;
			peakHourFinish=time;
			peakHourTasks=taskNo;
		}
		else if(taskNo==peakHourTasks){
			peakHourFinish = time;
		}
	}
	
	public static void setFinishTime(int t){
		simulationFinish = t;
	}
}
