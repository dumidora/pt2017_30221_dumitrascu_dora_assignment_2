import java.util.Random;

public class Task implements Comparable{
	private int arrTime;
	private int procTime;
	private int waitingTime;
	
	public Task(){}
	
	public Task(int arrTime, int procTime){ //interval for processing time
		this.arrTime=arrTime;
		this.procTime=procTime;
	}
	
	public void setWaitingTime(int waitingTime){
		this.waitingTime=waitingTime;
	}
	
	public int getWaitingTime(){
		return waitingTime;
	}
	
	public int getArrTime() {
		return arrTime;
	}
	public void setArrTime(int arrTime) {
		this.arrTime = arrTime;
	}
	public int getProcTime() {
		return procTime;
	}
	public void setProcTime(int procTime) {
		this.procTime = procTime;
	}
	
	@Override
	public int compareTo(Object o){
		int comp = ((Task)o).getArrTime();
		return this.getArrTime()-comp; 
		//sorting task
	}
	
	@Override
	public String toString(){
		return "Arrival: "+ arrTime + " ProcTime: "+procTime;
	}
}
