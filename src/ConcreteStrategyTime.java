import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ConcreteStrategyTime implements Strategy{

	@Override
	public void addTask(List <Server> servers, Task t){

		int min=servers.get(0).getWaitingPeriod().get();

		int minIndex=0;
		for(Server m : servers){
			if(m.getWaitingPeriod().doubleValue()<min){ //convert AtomicInteger to double so we can compare to each other
				int newVal = m.getWaitingPeriod().get();
				min=newVal;
				minIndex=servers.indexOf(m);
			}
			
		}
		servers.get(minIndex).addTask(t);
	}
}

