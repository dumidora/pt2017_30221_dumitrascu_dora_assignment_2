import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable{
	private BlockingQueue<Task> tasks;
	private AtomicInteger waitingPeriod= new AtomicInteger();
	protected boolean done = false;
	
	public Server(int size){
		tasks = new LinkedBlockingQueue<Task>(size);
	}

	
	public AtomicInteger getWaitingPeriod() {
		return waitingPeriod;
	}

	public void setWaitingPeriod(AtomicInteger waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}
	
	public Task[] getTasks() {
		Task[] taskArray = new Task[tasks.size()];
		tasks.toArray(taskArray);
		return taskArray;
	}
	
	public void removeTask(Task t){
		tasks.remove(t);
	}

	public void setTasks(BlockingQueue<Task> tasks) {
		this.tasks = tasks;
	}
	
	public void addTask(Task newTask){
		try{//adding new tasks to the servers a.k.a. clients enter the queue
			tasks.put(newTask); //this will be the client;
		/*
		 * the add method will throw IllegalStateException if no space is available
		 * put throws InterruptedException because if there is no space, it will wait to add a new task to the server
		 */
		waitingPeriod.addAndGet(newTask.getProcTime());
		}
		catch(InterruptedException e){
			e.printStackTrace();
		}
		
	}
	
	public void run() {
		System.out.println("Server running!");
		Task aux= new Task();
		while(!done){
			if(tasks.isEmpty()){
				//do nothing
			}
			else{
				aux=tasks.peek();
			try {
				System.out.println("I am trying to sleep for "+aux.getProcTime()+" seconds!");
				Thread.sleep((long)aux.getProcTime()*1000); //1000 to see simulation
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			try{
				tasks.take();
			}
			catch(InterruptedException e){
				e.printStackTrace();
			}
			waitingPeriod.addAndGet(-aux.getProcTime());
			}
		}
	}
	public void shutDown(){
		done = true;
	}
	
	public boolean returnStatus(){
		return done;
	}
	
	@Override
	public String toString(){
		return "Server length="+getTasks().length+"; Server waitingPeriod="+waitingPeriod;
	}
}
