import java.util.ArrayList;
import java.util.List;

public class Scheduler {
	private List<Server> servers;
	private int maxServers;
	private int maxTasksPerServer;
	private Strategy strategy = new ConcreteStrategyTime(); //default only to test
	
	
	
	public Scheduler(int maxServers, int maxTasksPerServer){
		servers= new ArrayList<Server>();
		for(int i=0;i<maxServers;i++){
			Server newServer = new Server(maxTasksPerServer);
			Thread t =new Thread(newServer);
			servers.add(newServer);
			t.start();
			
		}
	}
	
	public void addServer(Server s){
		servers.add(s);
	}
	
	
	public void changeStrategy(SelectionPolicy policy){
		if(policy == SelectionPolicy.SHORTEST_QUEUE){
			strategy = new ConcreteStrategyQueue();
		}
		
		if(policy == SelectionPolicy.SHORTEST_TIME){
			strategy = new ConcreteStrategyTime();
		}
	}
	
	public void dispatchTask(Task t){
		strategy.addTask(servers, t);
	}
	
	public List<Server> getServers(){
		return servers;
	}
	
	
}
